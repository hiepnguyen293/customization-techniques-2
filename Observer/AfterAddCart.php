<?php

namespace MagentoConfigEAV\ModuleHello\Observer;

use MagentoConfigEAV\ModuleHello\Helper\Data;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;

class AfterAddCart implements ObserverInterface
{
    /** @var ManagerInterface */
    protected $_customerSession;
    protected $messageManager;
    protected $helperData;

    /** @var UrlInterface */
    protected $url;

    /**
     * AfterAddCart constructor.
     * @param ManagerInterface $managerInterface
     * @param UrlInterface $url
     * @param Data $helperData
     */
    public function __construct(
        ManagerInterface $managerInterface,
        UrlInterface $url,
        Data $helperData,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_customerSession = $customerSession;
        $this->messageManager = $managerInterface;
        $this->url = $url;
        $this->helperData = $helperData;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $messageCollection = $this->messageManager->getMessages(true);
        $cartLink = '<a href="' . $this->url->getUrl('checkout/cart') . '">View Cart/Checkout</a>';
        $value = ' ';
        if(!$this->_customerSession->isLoggedIn())
        {
            $value = 'Dear guest, ';
        }
        else{
            $value = 'Dear ' . $this->_customerSession->getCustomer()->getName() . ',';
        }
        $this->messageManager->addSuccess($messageCollection->getLastAddedMessage()->getText() . '  ' . $value . ' ' . $cartLink);
    }
}
