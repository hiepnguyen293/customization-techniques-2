<?php

namespace TestRedirect\EventModule\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class UpdateMessage implements ObserverInterface
{
    /** @var \Magento\Framework\Message\ManagerInterface */
    protected $messageManager;

    /** @var \Magento\Framework\UrlInterface */
    protected $url;

    /** @var \Magento\CatalogInventory\Api\StockStateInterface */
    protected $_stockItem;

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;



    public function __construct(
        \Magento\Framework\Message\ManagerInterface $managerInterface,
        \Magento\Framework\UrlInterface $url,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\CatalogInventory\Api\StockStateInterface $stockItem
    ) {
        $this->messageManager = $managerInterface;
        $this->_stockItem = $stockItem;
        $this->url = $url;
        $this->_customerSession = $customerSession;
    }

    public function execute(Observer $observer)
    {

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $stockItem = $objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
        $productId = 1; // YOUR PRODUCT ID
        $productStock = $stockItem->get($productId);
        $b = $this->_customerSession->getCustomer()->getName();
//        var_dump($productStock->getData());die;
        $a = $productStock->getQty();
        $messageCollection = $this->messageManager->getMessages(true);
        $customMessage = 'Dear ' . $b . ' ' . 'Remaining quantity ' . '  ' . $a;
        $this->messageManager->addSuccess($messageCollection->getLastAddedMessage()->getText() . '  ' . $customMessage);
    }
}
