<?php

namespace MagentoConfigEAV\ModuleHello\Plugin;

use Magento\Framework\Message\ManagerInterface;
use MagentoConfigEAV\ModuleHello\Helper\Data;

class ExamplePlugin
{
    protected $_manager;
    protected $helperData;

    /**
     * Message constructor.
     *
     * @param ManagerInterface $manager
     * @param Data $helperData
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        ManagerInterface $manager,
        Data $helperData
    )
    {
        $this->_customerSession = $customerSession;
        $this->_manager = $manager;
        $this->helperData = $helperData;
    }
//    public function beforeSetTitle(\MagentoConfigEAV\ModuleHello\Controller\Index\Example $subject, $title)
//    {
//        $title = $title . " to ";
//        echo __METHOD__ . "</br>";
//
//        return [$title];
//    }


    public function beforeSetTitle(\MagentoConfigEAV\ModuleHello\Controller\Index\Example $subject, $result)
    {

        echo __METHOD__ . "</br>";
        echo $result;

        return '<h1>'. $result . 'Mageplaza.com' .'</h1>';

    }
}
