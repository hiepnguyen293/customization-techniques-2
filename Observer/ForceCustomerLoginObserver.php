<?php

namespace TestRedirect\EventModule\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ForceCustomerLoginObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;
    protected $_messageManager;
    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_customerSession = $customerSession;
        $this->redirect = $redirect;
        $this->_messageManager=$messageManager;

    }

    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $actionName = $observer->getEvent()->getRequest()->getFullActionName();
        $controller = $observer->getControllerAction();

        if($actionName == 'catalog_product_view')
        {
            $a = $this->_customerSession->getCustomer()->getName();
            if(!$this->_customerSession->isLoggedIn())
            {
                $message = 'DEAR GUEST, YOU MUST LOGIN FIRST';
                $this->redirect->redirect($controller->getResponse(), 'customer/account/login');
                $this->_messageManager->addError($message);
            }
            else
            {
                $message = 'Dear ' . $a . ' customer, You have logged in';
                $this->redirect->redirect($controller->getResponse(), 'customer/account/login');
                $this->_messageManager->addSuccess($message);
            }
        }
    }
}
