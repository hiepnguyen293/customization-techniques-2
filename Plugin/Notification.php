<?php

namespace MagentoConfigEAV\ModuleHello\Plugin;

use MagentoConfigEAV\ModuleHello\Helper\Data;
use Magento\Framework\Message\Manager;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Message\MessageInterface;

class Notification
{
    protected $_manager;
    protected $helperData;
    protected $_customerSession;

    /**
     * Message constructor.
     *
     * @param ManagerInterface $manager
     * @param Data $helperData
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        ManagerInterface $manager,
        Data $helperData
    )
    {
        $this->_customerSession = $customerSession;
        $this->_manager = $manager;
        $this->helperData = $helperData;
    }

    public function beforeaddSuccessMessage(
        \Magento\Framework\Message\Manager $subject,
        $message,
        $group = null)
    {
        $value = '';
        if(!$this->_customerSession->isLoggedIn())
        {
            $value = 'Dear guest, ';
        }
        else{
            $value = 'Dear ' . $this->_customerSession->getCustomer()->getName() . ',';
        }
        $message = $value . ' , ' . $message;
        return $message;
    }

    public function addErrorMessage(
        \Magento\Framework\Message\Manager $subject,
        $message,
        $group = null)
    {
        $value = '';
        if(!$this->_customerSession->isLoggedIn())
        {
            $value = 'Dear guest, ';
        }
        else{
            $value = 'Dear ' . $this->_customerSession->getCustomer()->getName() . ',';
        }
        $message = $value . ' , ' . $message;
        return $message;
    }

}
